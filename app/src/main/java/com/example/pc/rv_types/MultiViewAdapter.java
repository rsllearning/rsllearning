package com.example.pc.rv_types;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class MultiViewAdapter extends RecyclerView.Adapter {

    ArrayList<UserBean> listBeans;
    Context context;
    UserBean userBean;

    public MultiViewAdapter(ArrayList<UserBean> listBeans, Context context) {
        this.listBeans = listBeans;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case UserBean.TYPE_1:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout1, parent, false);
                return new MyVH1(view);
            case UserBean.TYPE_2:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout2, parent, false);
                return new MyVH2(view);
            default:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout2, parent, false);
                return new MyVH2(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        userBean = listBeans.get(position);
        if (userBean != null) {
            switch (userBean.type) {
                case UserBean.TYPE_1:
                    ((MyVH1) holder).tvName.setText(userBean.getUserName());
                    ((MyVH1) holder).tvDetails.setText(userBean.getUserDetail());
                    break;
                case UserBean.TYPE_2:
                    ((MyVH2) holder).tvName.setText(userBean.getUserName());
                    ((MyVH2) holder).tvDetails.setText(userBean.getUserDetail());
                    break;
            }
        }
    }

    @Override
    public int getItemViewType(int position) {

        switch (listBeans.get(position).type) {
            case 0:
                return UserBean.TYPE_1;
            case 1:
                return UserBean.TYPE_1;
            default:
                return -1;
        }
    }

    @Override
    public int getItemCount() {
        return listBeans.size();
    }
}
