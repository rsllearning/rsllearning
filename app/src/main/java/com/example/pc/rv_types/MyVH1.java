package com.example.pc.rv_types;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

public class MyVH1 extends RecyclerView.ViewHolder{

    TextView tvName, tvDetails;

    public MyVH1(View itemView) {
        super(itemView);
        this.tvName = (TextView) itemView.findViewById(R.id.tv_name);
        this.tvDetails = (TextView) itemView.findViewById(R.id.tv_details);
    }
}
