package com.example.pc.rv_types;

public class UserBean {

    public String userName, userDetail;
    public static final int TYPE_1 = 1;
    public static final int TYPE_2 = 2;
    public int type;

    public UserBean(int type, String userName, String userDetail) {
        this.type = type;
        this.userName = userName;
        this.userDetail = userDetail;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserDetail() {
        return userDetail;
    }

    public void setUserDetail(String userDetail) {
        this.userDetail = userDetail;
    }
}
