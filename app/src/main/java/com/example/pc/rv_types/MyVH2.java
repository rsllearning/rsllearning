package com.example.pc.rv_types;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

public class MyVH2 extends RecyclerView.ViewHolder{

    TextView tvName, tvDetails;

    public MyVH2(View itemView) {
        super(itemView);
        tvName = (TextView) itemView.findViewById(R.id.tv_name_2);
        tvDetails = (TextView) itemView.findViewById(R.id.tv_details_2);
    }
}
